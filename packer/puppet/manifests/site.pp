node 'packer-virtualbox-iso' {
  include openjdk::packages
  include openjdk::admin_user
  include openjdk::sudo
  include openjdk::install_betterrev
}
