define download_file(
        $site="",
        $cwd="",
        $user="") {                                                                                         
        
    exec { $name:                                                                                                                     
        command => "wget -O ${name} '${site}'",                                                         
        cwd => $cwd,
        path => "/usr/bin/",
        creates => "${cwd}/${name}",     
        user => $user,          
    }

}

define unzip(
        $file="",
        $cwd="",
        $user="") {                                                                                         

    exec { $name:                                                                                                                     
        command => "unzip -d ${name} ${file}",                                                         
        cwd => $cwd,
        path => "/usr/bin/",
        creates => "${cwd}/${name}",                                                              
        user => $user,                                                                                                          
    }
}


class openjdk::install_betterrev {

  $version = "1.0.0-SNAPSHOT"
  $deployment_dir = "/home/betterrev/betterrev"
  
  file { $deployment_dir:
    ensure => "directory",
    owner => "betterrev",
    group  => "betterrev",
  }

  file { "${deployment_dir}/logs":
    ensure => "directory",
    owner => "betterrev",
    group  => "betterrev",
  }

  download_file { "betterev.zip":
    site => "https://bitbucket.org/adoptopenjdk/betterrev/downloads/betterev-${version}.zip",
    cwd  => $deployment_dir,
    user => "betterrev"
  }
  
  unzip { "deployment":
    cwd  => $deployment_dir,
    file => "betterev.zip",
    user => "betterrev"
  }

  file { "/etc/init/betterrev.conf":
    content => template("openjdk/betterrev.conf"),
    owner  => "root",
    group  => "root",
  }

  file { "/etc/init.d/betterrev":
    ensure => link,
    target => "/lib/init/betterrev",
  }

  service { "betterrev":
    ensure => running,
    provider => "upstart",
    require => File["/etc/init.d/betterrev"],
  }
}