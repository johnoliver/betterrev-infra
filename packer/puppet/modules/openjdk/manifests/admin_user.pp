class openjdk::admin_user {
  user { 'devadmin':
    groups      => ['admin'],
    require     => Group['admin'],
    shell       => '/bin/bash',
    name        => 'devadmin',
    ensure      => present, 
    password    => '$6$IBS0wmrDu7w/a$/1W8VzdXx6gEXspVavqi6g8VCjs3JdByufU/1F8HrZMbbFdddK2FqdPn48wuBS073CVPJu1QwIk.t4.1jnOS00',
    home        => '/home/devadmin/',
    managehome  => true,
  }
  
  user { 'betterrev':
    shell       => '/bin/bash',
    name        => 'betterrev',
    ensure      => present, 
    password    => '*',
    home        => '/home/betterrev/',
    managehome  => true,
  }
}