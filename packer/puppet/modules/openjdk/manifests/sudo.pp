class openjdk::sudo {

  group { 'admin': ensure  => present, }

  file  { '/etc/sudoers':
    source => 'puppet:///modules/openjdk/etc_sudoers',
    owner  => 'root',
    group  => 'root',
  }

}
